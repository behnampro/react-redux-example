import {BUY_MILK} from "../constants/MilkTypes";

export const buyMilk = () => {
    return {
        type : BUY_MILK,
    }
}