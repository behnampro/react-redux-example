import React, {useState} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {buyCake} from "../actions";

function FlexCakeContainer() {

    const [number, setNumber] = useState(1);
    const numOfCake = useSelector(state => state.cake.numOfCake);
    const dispatch = useDispatch(number);

    return (
        <div>
            <h1> Number of CAke {numOfCake}</h1>
            <input type="text" value={number} onChange={(e) => setNumber(e.target.value)}/>
            <button onClick={() => dispatch(buyCake(number))}> Buy Cake </button>
        </div>
    )
}

export default FlexCakeContainer