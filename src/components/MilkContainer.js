import React from "react";
import {buyMilk} from "../actions";
import {connect} from "react-redux";

function MilkContainer(props) {
    return (
        <div>
            <h1> Number of Milks {props.numOfMilk}</h1>
            <button onClick={props.buyMilk}> Buy Milk </button>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        numOfMilk: state.milk.numOfMilk
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        buyMilk : () => dispatch(buyMilk())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MilkContainer);