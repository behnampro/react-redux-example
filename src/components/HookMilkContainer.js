import React from "react";
import {useDispatch, useSelector} from 'react-redux'
import {buyMilk} from "../actions";

function HookMilkContainer() {

    const numOfMilk = useSelector(state => state.milk.numOfMilk);
    const dispatch = useDispatch()

    return (
        <div>
            <h1> Number of Milks {numOfMilk}</h1>
            <button onClick={() => dispatch(buyMilk())}> Buy Milk </button>
        </div>
    )
}

export default HookMilkContainer;