import React from 'react';
import MilkContainer from "./components/MilkContainer";
import './App.css';
import {Provider} from "react-redux";
import store from "./store/store";
import HookMilkContainer from "./components/HookMilkContainer";
import HookCakeContainer from "./components/HookCakeContainer";
import FlexCakeContainer from "./components/FlexCakeContainer";


function App() {
  return (
      <Provider store={store}>
          <div className="App">
              <HookMilkContainer />
              <hr />
              <HookCakeContainer />
              <hr />
              <FlexCakeContainer />
              <hr />
              <MilkContainer />
          </div>
      </Provider>
  );
}

export default App;
