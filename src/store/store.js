import {createStore, combineReducers, applyMiddleware} from "redux";
import MilkReducer from "../reducers/MilkReducers";
import CakeReducer from "../reducers/CakeReducers";
import logger from 'redux-logger';
import {composeWithDevTools} from "redux-devtools-extension";

const rootReducer = combineReducers({
    cake: CakeReducer,
    milk: MilkReducer
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(logger)));

export default store;