import {BUY_CAKE} from "../constants/CakeTypes";

const initialState = {
    numOfCake : 25
}

const CakeReducer = (state= initialState, action) => {
    switch (action.type) {
        case BUY_CAKE :
            return {
                ...state,
                numOfCake: state.numOfCake - action.payload
            }
        default :
            return state;
    }
}

export default CakeReducer;