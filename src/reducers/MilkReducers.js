import {BUY_MILK} from "../constants/MilkTypes";

const initialState = {
    numOfMilk : 20
}

const MilkReducer = (state= initialState, action) => {
    switch (action.type) {
        case BUY_MILK :
            console.log("error");
            return {
                ...state,
                numOfMilk: state.numOfMilk - 1
            }
        default :
            return state;
    }
}

export default MilkReducer;